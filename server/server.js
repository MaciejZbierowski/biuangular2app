var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var httpServer = require('http').Server(app);
var cors = require('cors');
var port = process.env.PORT || 5000;

var index = require('./routes/index');
var students = require('./routes/students');
var marks = require('./routes/marks');
var grades = require('./routes/grades');

var mongoose = require('mongoose');
mongoose.connect('mongodb://mongouser:mongopassword@ds041992.mongolab.com:41992/appdb');

mongoose.connection.on('connected', function () {
    var Grade = require('./models/grade');

    Grade.remove(function(err,removed) {

        console.log('Grades removed success');
    });

    var grade1 = new Grade();
    grade1.name = "1A";
    grade1.level = 1;

    var grade2 = new Grade();
    grade2.name = "1B";
    grade2.level = 1;

    var grade3 = new Grade();
    grade3.name = "2A";
    grade3.level = 2;

    var grades = [grade1, grade2, grade3];
    grades.forEach(function(grade) {
        grade.save(function(err) {
            if(err){
                res.send(err);
            }
            console.log('Grade created ' + grade.name);
        });

    });
    
    var Student = require('./models/student');

    Student.remove(function(err,removed) {
        console.log('Students removed success');
    });
    
    var student1 = new Student();
    student1.name = "name1";
    student1.lastName = "lastname1";
    student1.email = "student1@wp.pl";
    student1.grade = grade1;

    var student2 = new Student();
    student2.name = "name2";
    student2.lastName = "lastname2";
    student2.email = "student2@wp.pl";
    student2.grade = grade1;

    var student3 = new Student();
    student3.name = "name3";
    student3.lastName = "lastname3";
    student3.email = "student3@wp.pl";
    student3.grade = grade2;
    
    var students = [student1, student2, student3];
    students.forEach(function(student) {
        student.save(function(err) {
            if(err){
                res.send(err);
            }
            console.log('Student created ' + student.name);
        });

    });
    
    var Mark = require('./models/mark');

    Mark.remove(function(err,removed) {
        console.log('Marks removed success');
    });
    
    var mark1 = new Mark();
    mark1.value = "value1";
    mark1.weight = "weight1";
    mark1.description = "des1";
    mark1.student = student1;
    
    var mark2 = new Mark();
    mark2.value = "value2";
    mark2.weight = "weight2";
    mark2.description = "des2";
    mark2.student = student1;
    
    var mark3 = new Mark();
    mark3.value = "value3";
    mark3.weight = "weight3";
    mark3.description = "des3";
    mark3.student = student2;
    
    // var marks = [mark1, mark2, mark3];
    // marks.forEach(function(mark) {
    //     mark.save(function(err) {
    //         if(err){
    //             res.send(err);
    //         }
    //         console.log('Mark created ' + mark.value);
    //     });

    // });
});

app.use(cors({
    allowedOrigins: [
        'http://localhost:3000'
    ]
}));

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use('/', index);
app.use('/api', students);
app.use('/api', marks);
app.use('/api', grades);

httpServer.listen(port, function(){
    console.log('App listens on localhost: ' + port);
});