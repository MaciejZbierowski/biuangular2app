import { Student } from './student';

export class Mark {
    _id: string;
    value: number;
    weight: number;
    description : string;
    
}