import { Component, NgZone } from '@angular/core';
import { JSONP_PROVIDERS }  from '@angular/http';
import { Observable }       from 'rxjs/Observable';

import { TestService } from './test.service';
import {Dragula, DragulaService} from 'ng2-dragula/ng2-dragula';

//import { UPLOAD_DIRECTIVES } from 'ng2-uploader/ng2-uploader';

@Component({
  selector: 'my-wiki',
  templateUrl: 'app/components/test/test.component.html',
  providers: [JSONP_PROVIDERS, TestService, DragulaService],
  directives:[Dragula],
  styleUrls: ['app/components/test/test.css']
  
})
export class TestComponent {
    
    items: Observable<string[]>;
    words = new Array<string>();
    word: string;
    sentence = Array<string>();
    firstTask = false;
    secondTask = false;
    showTask1 = false;
    showTask2 = false;
    // zone: NgZone;
    // options: Object = {
    //     url: 'http://ng2-uploader.com:10050/upload'
    // };
    
    constructor (private testService: TestService, private dragulaService: DragulaService) {
        dragulaService.setOptions('third-bag', {
            removeOnSpill: true,
            direction: 'horizontal'
        });
        //this.zone = new NgZone({ enableLongStackTrace: false });
    }
    
    search (term: string) {
        this.items = this.testService.search(term);
    }
    
    onClick(item: string){
        this.words.push(item);
    }
    
    vocabularyTask(){
        this.firstTask = true;
        this.showTask1 = true; 
    }
    
    saveFirst(){
        this.showTask1 = false;
    }
    
    orderTask(){
        this.showTask2 = true;
        this.secondTask = true;
    }
    
    makeSentence(sentence: string){
        this.sentence = sentence.match(/\S+\s*/g);
        
    }
}