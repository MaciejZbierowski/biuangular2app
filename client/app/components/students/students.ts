import { Component, OnInit, provide } from '@angular/core';
import { Router } from '@angular/router-deprecated';

import { Student } from '../../models/student';
import { StudentService } from '../../services/student';
import { StudentDetailComponent } from '../student-detail/student-detail';
import { Mark } from '../../models/mark';
import { SelectStudentComponent } from '../select-student/select-student';
import { StudentFormComponent } from '../student-form/student-form';
import {GradeService} from "../../services/grade";
import {Grade} from "../../models/grade";

 import {Confirm, ConfirmOptions, Position} from 'angular2-bootstrap-confirm/';
import {PositionService} from 'angular2-bootstrap-confirm/position/position';

@Component({
    selector: 'my-students',
    templateUrl: 'app/components/students/students.html',
    directives: [Confirm, StudentDetailComponent, SelectStudentComponent, StudentFormComponent],
    providers: [ConfirmOptions, GradeService, StudentService, provide(Position, {useClass: PositionService})]
})

export class StudentsComponent implements OnInit {
    
    public title: string = 'Popover title';
  public message: string = 'Popover description';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  public isOpen: boolean = false;
    
    students = new Array<Student>();
    selectedStudent: Student;
    aStudent = false;
    grades = new Array<Grade>();
    removeUser = false;
    
    constructor(private router: Router, private studentService: StudentService, private gradeService: GradeService){}
    
    getStudents(gradeId: string){
        this.studentService.getStudents(gradeId).subscribe(
            students => this.students = students,
            error => console.log(<any>error)
        )
    }
    
    addStudent(){
        this.aStudent = true;
        this.selectedStudent = null;
    }
    
    delete(studentId: string) {
        
        //console.log('confirmClicked ==' + this.confirmClicked + ', cancelClicked == ' + this.cancelClicked);
        
        var deleteUSer = window.confirm('Are you absolutely sure you want to delete?');
        console.log('deleteUSer == ' + deleteUSer);
        
        if(deleteUSer === true) {
            this.studentService.deleteStudent(studentId).subscribe(
            students => this.students.forEach((t, index) => {
                if(t._id === studentId) { this.students.splice(index,1);}
            }),
            error =>  console.log(<any>error)
            );
        } else {
            console.log('nie usuwaj');
        }
        
        // if(this.confirmClicked){
        //     console.log('clicked confirmed');
        // }
        
        
        
    }
    
    gotoDetail(student: Student){
        let link = ['StudentDetail', {id: student._id}];
        this.router.navigate(link);
    }

    onSelect(student: Student){
        this.selectedStudent = student;
        this.aStudent = false;
    }
    
    ngOnChanges(): any{
    }

    getGrades() {
        this.gradeService.getGrades().subscribe(
            grades => this.grades = grades,
            error => console.log(<any>error)
        );
    }
    
    ngOnInit(): any{
        this.getStudents(null);
        this.getGrades();
    }
    
};