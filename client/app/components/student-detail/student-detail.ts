import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { Student } from '../../models/student';
import { StudentService } from '../../services/student';

@Component({
    selector: 'my-student-detail',
    templateUrl: 'app/components/student-detail/student-detail.html',
    providers: [StudentService]
})

export class StudentDetailComponent implements OnInit {
    @Input() student: Student;
    @Input() students: Student[];
    @Output() close = new EventEmitter();
    
    constructor(private studentService: StudentService, private routeParams: RouteParams){}
    
    ngOnInit(){
        this.studentService.getStudent(this.routeParams.get('id')).subscribe(student => this.student = student);
    }
    
    save(){
        this.studentService.putStudent(this.student).subscribe(
            student => this.student = student,
            error =>  console.log(<any>error)
        );
        this.goBack(this.student);
    }
    
    goBack(savedStudent: Student = null) {
        this.close.emit(savedStudent);
        window.history.back();
    }
}